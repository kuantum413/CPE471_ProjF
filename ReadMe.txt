Author: Keenan M. Reimer
Class: CPE 471
Professor: Zoe Wood
Assignment: Final Project
Video: https://www.youtube.com/watch?v=FvfuQ6weQAY

Special note: If you get an error about texture2D or texture in fading_tex_phong_frag.glsl change from one to the other.

Key commands:
w & s: move forward or backward
a & d: strafe left or right
space: move up
g: re-generate the world
up-arrow: add a shark randomly in the world

Mouse commands:
Left-Click: Drag to rotate camera

Features for CPE471:
- Hierarchically modelled shark animation
- Blinn-Phong Lighting
- Textured objects
- Distance alpha-fading
- Proceedurally generated world with lots of meshes
- Floating bubbles animation

Features from CPE474:
- Waving seaweed animation
